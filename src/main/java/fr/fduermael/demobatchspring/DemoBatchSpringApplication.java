package fr.fduermael.demobatchspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoBatchSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoBatchSpringApplication.class, args);
	}

}
